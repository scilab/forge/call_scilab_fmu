#ifndef __FMU2_HXX__
#define __FMU2_HXX__

extern "C"
{
#include "fmi2TypesPlatform.h"
#include "fmi2Functions.h"

#include <call_scilab.h>
#include <api_scilab.h>
#include <charEncoding.h>
#include <version.h>
}

#include "fmu_shared.hxx"

namespace call_scilab_fmu
{
    namespace fmu2
    {

        typedef call_scilab_fmu::CallScilabComponent<fmi2CallbackLogger, fmi2ComponentEnvironment> CallScilabComponent;

        template <fmi2Status level>
        fmi2Status report(CallScilabComponent *c, fmi2Status ret, const char *format, ...)
        {
            std::va_list args;
            va_start(args, format);

            std::string msg(256, '\0');
            int len = std::vsnprintf(msg.data(), msg.length(), format, args);
            if (len > 0 && (size_t)len > msg.length() - 1)
            {
                msg.resize(len + 1, '\0');
                len = std::snprintf(msg.data(), msg.length(), format, args);
            }

            va_end(args);

            c->logger(c->componentEnvironment, c->instanceName.c_str(), level, NULL, msg.data());
            if (ret < level)
                ret = level;
            return ret;
        }

    } /* namespace fmu2 */
}     /* namespace call_scilab_fmu */

#endif /* __FMU2_HXX__ */
