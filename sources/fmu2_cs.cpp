extern "C"
{
#include "fmi2TypesPlatform.h"
#include "fmi2Functions.h"

#include <call_scilab.h>
#include <api_scilab.h>
#include <charEncoding.h>
}

#include <string>
#include "fmu_shared.hxx"
#include "fmu2.hxx"

using namespace call_scilab_fmu::fmu2;

/***************************************************
Functions for FMI2 for Co-Simulation
****************************************************/

extern "C" fmi2Status fmi2SetRealInputDerivatives(fmi2Component c,
                                                  const fmi2ValueReference vr[], size_t nvr,
                                                  const fmi2Integer order[],
                                                  const fmi2Real value[])
{
    return fmi2OK;
}

extern "C" fmi2Status fmi2GetRealOutputDerivatives(fmi2Component c,
                                                   const fmi2ValueReference vr[], size_t nvr,
                                                   const fmi2Integer order[],
                                                   fmi2Real value[])
{
    return fmi2OK;
}

extern "C" fmi2Status fmi2DoStep(fmi2Component _c,
                                 fmi2Real currentCommunicationPoint,
                                 fmi2Real communicationStepSize,
                                 fmi2Boolean noSetFMUStatePriorToCurrentPoint)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    scilab_setVar(L"currentCommunicationPoint", scilab_createDouble(NULL, currentCommunicationPoint));
    scilab_setVar(L"communicationStepSize", scilab_createDouble(NULL, communicationStepSize));
    scilab_setVar(L"noSetFMUStatePriorToCurrentPoint", scilab_createBoolean(NULL, noSetFMUStatePriorToCurrentPoint));

    std::string cmd = "ModelVariables = fmi2DoStep(ModelVariables);";
    if (c->ExecuteScilabCommand(cmd))
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2DoStep() failed to execute.\n%s", c->formatError());
        if (ret < fmi2Error)
            ret = fmi2Error;
    }
    else if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
    }

    return ret;
}

extern "C" fmi2Status fmi2CancelStep(fmi2Component _c)
{
    return fmi2OK;
}

/*
 * used to communicate on asynchronous execution, this is not the case for now.
 */

extern "C" fmi2Status fmi2GetStatus(fmi2Component c, const fmi2StatusKind s,
                                    fmi2Status *value)
{
    return fmi2OK;
}
extern "C" fmi2Status fmi2GetRealStatus(fmi2Component c, const fmi2StatusKind s,
                                        fmi2Real *value)
{
    return fmi2OK;
}
extern "C" fmi2Status fmi2GetIntegerStatus(fmi2Component c, const fmi2StatusKind s,
                                           fmi2Integer *value)
{
    return fmi2OK;
}
extern "C" fmi2Status fmi2GetBooleanStatus(fmi2Component c, const fmi2StatusKind s,
                                           fmi2Boolean *value)
{
    return fmi2OK;
}
extern "C" fmi2Status fmi2GetStringStatus(fmi2Component c, const fmi2StatusKind s,
                                          fmi2String *value)
{
    return fmi2OK;
}
