#ifndef __FMU_SHARED_HXX__
#define __FMU_SHARED_HXX__

extern "C"
{
#include <stdarg.h>

#include <sci_malloc.h>
#include <call_scilab.h>
#include <api_scilab.h>
#include <charEncoding.h>
#include <version.h>
}

#include <vector>
#include <string>
#include <filesystem>
#include <cstdio>
#include <cstdarg>

#include <scilabWrite.hxx>

namespace call_scilab_fmu
{
    struct owned_string_t
    {
        char *_str;

        explicit owned_string_t(const wchar_t *wide) : _str(wide_string_to_UTF8(wide))
        {
        }

        explicit owned_string_t(const char *str) : _str((char *)str)
        {
        }

        explicit owned_string_t(char *str) : _str(str)
        {
        }

        ~owned_string_t()
        {
            if (_str)
                FREE(_str);
        }

        owned_string_t(const owned_string_t &str) = delete;
        owned_string_t(owned_string_t &&str) : _str(str.data()) { str._str = NULL; };
        owned_string_t &operator=(const owned_string_t &str) = delete;
        owned_string_t &operator=(owned_string_t &&str)
        {
            _str = str._str;
            str._str = NULL;
            return *this;
        };

        const char *c_str()
        {
            return _str;
        }

        char *data()
        {
            return _str;
        }
    };

    template <typename callbackLogger_t, typename componentEnvironment_t>
    class CallScilabComponent
    {
    public:
        static inline CallScilabComponent<callbackLogger_t, componentEnvironment_t> *current;

        const std::string instanceName;
        const callbackLogger_t logger;
        const componentEnvironment_t componentEnvironment;

        int loggingOn;

        const std::filesystem::path resourcesPath;

        // transient buffer used for logging
        std::string logger_buffer;
        std::vector<owned_string_t> returnedString;

        // transient Scilab context to handle multiple FMUs
        const scilabEnv context;

    private:
        static void scilabErrLogger(const char *text)
        {
            current->logger_buffer.append(text);
        }
        static void scilabOutLogger(const char *text)
        {
            current->logger_buffer.append(text);
        }

    public:
        CallScilabComponent(const char *_instanceName,
                            callbackLogger_t _logger,
                            componentEnvironment_t _componentEnvironment,
                            int _loggingOn,
                            const std::filesystem::path &_resourcesPath,
                            const scilabEnv _context) : instanceName(_instanceName),
                                              logger(_logger),
                                              componentEnvironment(_componentEnvironment),
                                              loggingOn(_loggingOn),
                                              resourcesPath(_resourcesPath),
                                              logger_buffer(),
                                              context(_context){};

        // execute a Scilab command on a component
        int ExecuteScilabCommand(std::string &cmd)
        {
            current = this;
            if (loggingOn)
            {
                setScilabErrorStreamMethod(scilabErrLogger);
                setScilabOutputStreamMethod(scilabOutLogger);
            }

            logger_buffer.clear();
#if SCI_VERSION_MAJOR >= 2024 && SCI_VERSION_MINOR >= 1
            char *msg = NULL;
            char *stacktrace = NULL;
            int ret = ExecScilabCommand(cmd.data(), &msg, &stacktrace);
            if (ret)
            {
                returnedString.emplace_back(msg);
                returnedString.emplace_back(stacktrace);
            }
#else
            int ret = SendScilabJob(cmd.data());
#endif

            current = nullptr;
            return ret;
        }

        // output the Scilab error string (no need to deallocate)
        const char *formatError()
        {
#if SCI_VERSION_MAJOR >= 2024 && SCI_VERSION_MINOR >= 1
            logger_buffer.append(returnedString.back().c_str());
            returnedString.pop_back();
            logger_buffer.append(returnedString.back().c_str());
            returnedString.pop_back();
#else
            char *lastErr = getLastErrorMessageSingle();
            logger_buffer.append(lastErr);
            FREE(lastErr);
#endif

            return logger_buffer.c_str();
        }

    }; /* class CallScilabComponent */

    // fill a scalar from a Scilab variable
    template <typename T>
    int mapScalarScilabVariable(const wchar_t *wideVarName, int isT(scilabEnv, scilabVar), scilabStatus getT(scilabEnv, scilabVar, T *v), T *out)
    {
        scilabVar var = scilab_getVar(wideVarName);
        if (scilab_isScalar(NULL, var) && isT(NULL, var))
        {
            getT(NULL, var, out);
            return 0;
        }
        else
        {
            return 1;
        }
    }

    // fill vector from a Scilab variable
    template<typename T, int isT(scilabEnv, scilabVar), scilabStatus getTArray(scilabEnv, scilabVar, T**)>
    int mapVectorScilabVariable(const wchar_t *wideVarName, T *out, size_t n_out)
    {
        scilabVar var = scilab_getVar(wideVarName);
        if (isT(NULL, var))
        {
            T *values = NULL;
            getTArray(NULL, var, &values);

            size_t len = std::min(n_out, (size_t)scilab_getSize(NULL, var));
            for (size_t i = 0; i < len; ++i)
            {
                out[i] = values[i];
            }
        }
        else
        {
            return 1;
        }
        return 0;
    }

    // fill double vector from a Scilab variable
    inline int mapDoubleVectorScilabVariable(const wchar_t *wideVarName, double *out, size_t n_out)
    {
        return mapVectorScilabVariable<double, scilab_isDouble, scilab_getDoubleArray>(wideVarName, out, n_out);
    }

} /* namespace call_scilab_fmu */

#endif /* __FMU_SHARED_HXX__ */