extern "C"
{
#include "fmi2TypesPlatform.h"
#include "fmi2Functions.h"

#include <call_scilab.h>
#include <api_scilab.h>
#include <charEncoding.h>
#include <version.h>
}

#ifdef _MSC_VER
#define NOMINMAX
#include <windows.h>
#undef NOMINMAX
#else
#include "dlfcn.h"
#endif /* _MSC_VER */

#include <new>
#include <string>
#include <vector>
#include <filesystem>

#include "fmu_shared.hxx"
#include "fmu2.hxx"

using namespace call_scilab_fmu::fmu2;

namespace
{
    // helper to get and convert a string from Scilab
    scilabStatus getString(scilabEnv env, scilabVar var, fmi2String *v)
    {
        wchar_t *returned;
        if (scilab_getString(env, var, &returned))
        {
            return STATUS_ERROR;
        }
        if (returned == NULL)
        {
            return STATUS_ERROR;
        }
        CallScilabComponent::current->returnedString.emplace_back(returned);
        *v = CallScilabComponent::current->returnedString.back().data();
        return STATUS_OK;
    };

    // helper to set and create a string into Scilab
    scilabVar createString(scilabEnv env, fmi2String v)
    {
        scilabVar var;
        if (v != NULL)
        {
            wchar_t *wide = to_wide_string(v);
            var = scilab_createString(env, wide);
            FREE(wide);
        }
        else
        {
            var = scilab_createString(env, L"");
        }
        return var;
    }

    // helper function template for getting a value
    template <typename T>
    fmi2Status fmi2GetXXX(const std::string &fname, char refType, CallScilabComponent *c, const unsigned int vr[], size_t nvr, T value[], int isT(scilabEnv, scilabVar), scilabStatus getT(scilabEnv, scilabVar, T *), scilabVar createT(scilabEnv, T), fmi2Status reportError(CallScilabComponent *c, fmi2Status, const char *format, ...))
    {
        fmi2Status ret = fmi2OK;

        scilabVar list = scilab_getVar(L"ModelVariables");
        if (list == NULL)
        {
            ret = reportError(c, ret, "%s() failed, ModelVariables does not exist.", fname.c_str());
            return ret;
        }

        for (size_t i = 0; i < nvr; ++i)
        {
            // 1-indexed for Scilab internal list
            if (vr[i] < 1)
            {
                ret = reportError(c, ret, "%s() failed, #%c%d# valueReference should be 1 or more, got %d.", fname.c_str(), refType, vr[i], vr[i]);
                continue;
            }
            if (vr[i] > (size_t) std::numeric_limits<int>::max())
            {
                ret = reportError(c, ret, "%s() failed, #%c%d# valueReference less than %d, got %d.", fname.c_str(), refType, vr[i], std::numeric_limits<int>::max(), vr[i]);
                continue;
            }
            int index = (int) (vr[i] - 1);

            if (index < scilab_getSize(NULL, list) && scilab_isDefined(NULL, list, index))
            {
                scilabVar v = scilab_getListItem(NULL, list, index);

                if (!scilab_isScalar(NULL, v))
                {

                    ret = reportError(c, ret, "%s() failed, #%c%d# ModelVariables(%d) is not a scalar.", fname.c_str(), refType, vr[i], vr[i] + 1);
                    continue;
                }
                if (!isT(NULL, v))
                {
                    ret = reportError(c, ret, "%s() failed, #%c%d# ModelVariables(%d) type is not %s.", fname.c_str(), refType, vr[i], vr[i] + 1, typeid(T).name());
                    continue;
                }
                if (getT(NULL, v, &value[i]))
                {
                    ret = reportError(c, ret, "%s() failed, #%c%d# ModelVariables(%d) cannot be read.", fname.c_str(), refType, vr[i], vr[i] + 1);
                    continue;
                }
            }
            else
            {
                T v{};
                // set default value on Scilab-side if it does not exist yet
                scilab_setListItem(NULL, list, index, createT(NULL, v));
                value[i] = v;
            }
        }
        return ret;
    }

    // helper function template for setting a value
    template <typename T>
    fmi2Status fmi2SetXXX(const std::string &fname, char refType, CallScilabComponent *c, const unsigned int vr[], size_t nvr, const T value[], scilabVar createT(scilabEnv, T), fmi2Status reportError(CallScilabComponent *c, fmi2Status, const char *format, ...))
    {
        fmi2Status ret = fmi2OK;

        scilabVar list = scilab_getVar(L"ModelVariables");
        if (list == NULL)
        {
            ret = reportError(c, ret, "%s() failed, ModelVariables does not exist.", fname.c_str());
            return ret;
        }

        for (size_t i = 0; i < nvr; ++i)
        {
            // 1-indexed for Scilab internal list
            if (vr[i] < 1)
            {
                ret = reportError(c, ret, "%s() failed, #%c%d# valueReference should be 1 or more, got %d.", fname.c_str(), refType, vr[i], vr[i]);
                continue;
            }
            if (vr[i] > (size_t) std::numeric_limits<int>::max())
            {
                ret = reportError(c, ret, "%s() failed, #%c%d# valueReference less than %d, got %d.", fname.c_str(), refType, vr[i], std::numeric_limits<int>::max(), vr[i]);
                continue;
            }
            int index = (int) (vr[i] - 1);

            scilabVar v = createT(NULL, value[i]);
            if (v == NULL)
            {
                ret = reportError(c, ret, "%s() failed, unable to create #%c%d# ModelVariables(%d).", fname.c_str(), refType, vr[i], vr[i] + 1);
                continue;
            }

            // switch to 0-indexed for Scilab internal list
            if (scilab_setListItem(NULL, list, index, v))
            {
                ret = reportError(c, ret, "%s() failed, unable to set #%c%d# ModelVariables(%d).", fname.c_str(), refType, vr[i], vr[i] + 1);
                continue;
            }
        }
        return ret;
    };

} /* namespace */

/***************************************************
Common Functions
****************************************************/
extern "C" const char *fmi2GetTypesPlatform(void)
{
    return fmi2TypesPlatform;
}

extern "C" const char *fmi2GetVersion(void)
{
    return fmi2Version;
}

extern "C" fmi2Component fmi2Instantiate(fmi2String instanceName,
                                         fmi2Type fmuType,
                                         fmi2String fmuGUID,
                                         fmi2String fmuResourceLocation,
                                         const fmi2CallbackFunctions *functions,
                                         fmi2Boolean visible,
                                         fmi2Boolean loggingOn)
{
    // from call_scilab documentation, the SCI env. variable real impact is unclear (vs LDFLAGS, PATH usage)
    if (Call_ScilabOpen(getenv("SCI"), TRUE, NULL, 0) > 0)
    {
        functions->logger(functions->componentEnvironment, instanceName, fmi2Error, NULL, "StartScilab() failed, check your SCI and PATH env. variables.");
        return NULL;
    }

    // the resourcesPath contains the Scilab script, it should be a file:/
    if (std::string(fmuResourceLocation).rfind("file:/", 0) != 0)
    {
        functions->logger(functions->componentEnvironment, instanceName, fmi2Error, NULL, "StartScilab() failed, \"file:/\" fmuResourceLocation expected.");
        return NULL;
    }
    const char *resourcesPath = fmuResourceLocation + 6;
    // could be either file:///C:/temp/MyFMU/resources or file:/C:/temp/MyFMU/resources
    while (resourcesPath[0] == '/' && resourcesPath[1] == '/')
    {
        resourcesPath++;
    }
    // skip the first '/' for windows
#ifdef _MSC_VER
    while (resourcesPath[0] == '/')
    {
        resourcesPath++;
    }
#endif
    std::filesystem::path resources(resourcesPath);
    if (!std::filesystem::is_directory(resources))
    {
        functions->logger(functions->componentEnvironment, instanceName, fmi2Error, NULL, "StartScilab() failed, fmuResourceLocation \"%s\" is not a directory.", resourcesPath);
        return NULL;
    }

    // check and configure the visible flag
    if (ScilabHaveAGraph() == FALSE && visible)
    {
        functions->logger(functions->componentEnvironment, instanceName, fmi2Error, NULL, "StartScilab() failed, visible requested but Scilab has been built without GUI.");
        return NULL;
    }
    // visible is not really accessible, disable it and make a Scilab with GUI
    // if (!visible)
    // {
    //     DisableInteractiveMode();
    // }

    // API Scilab context is not handled
    scilabEnv context = NULL;

    // we don't really respect allocateMemory/freeMemory, as any Scilab script will allocate
    CallScilabComponent *c = new CallScilabComponent(instanceName, functions->logger, functions->componentEnvironment, loggingOn, resourcesPath, context);

    return c;
}

extern "C" void fmi2FreeInstance(fmi2Component _c)
{
    auto c = (CallScilabComponent *)_c;
    TerminateScilab(NULL);
    delete c;
}

extern "C" fmi2Status fmi2SetDebugLogging(fmi2Component _c, fmi2Boolean loggingOn, size_t nCategories, const fmi2String categories[])
{
    auto c = (CallScilabComponent *)_c;
    c->loggingOn = loggingOn;

    if (nCategories > 1)
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Warning, NULL, "fmi2SetDebugLogging() failed, LogCategories are not supported.");
        return fmi2Warning;
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2SetupExperiment(fmi2Component _c, fmi2Boolean toleranceDefined, fmi2Real tolerance, fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    // setup arguments as Scilab variables
    if (toleranceDefined)
    {
        scilab_setVar(L"tolerance", scilab_createDouble(NULL, tolerance));
    }
    scilab_setVar(L"startTime", scilab_createDouble(NULL, startTime));
    if (stopTimeDefined)
    {
        scilab_setVar(L"stopTime", scilab_createDouble(NULL, stopTime));
    }

    // load all macros on resources/
    std::string cmd;
    cmd += "getd(\"";
    cmd += c->resourcesPath.string();
    cmd += "\");";
    if (c->ExecuteScilabCommand(cmd))
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Warning, NULL, "fmi2SetupExperiment() failed, resources directory \"%s\" cannot be loaded.\n%s", c->resourcesPath.c_str(), c->formatError());
        if (ret < fmi2Warning)
            ret = fmi2Warning;
    }
    else if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
    }

    // load all macros on CWD, this is very usefull to debug and is an acceptable error
    auto p = std::filesystem::current_path().string();
    cmd = "getd(\"" + p + "\");";
    if (c->ExecuteScilabCommand(cmd))
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, "fmi2SetupExperiment() warned, current directory \"%s\" cannot be loaded.\n%s", p.c_str(), c->formatError());
    }
    else if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
    }

    // execute all scripts on resources/
    cmd = "exec(ls(\"" + p + "/*.sce\"));";
    if (c->ExecuteScilabCommand(cmd))
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, "fmi2SetupExperiment() warned, current directory \"%s\" cannot be loaded.\n%s", p.c_str(), c->formatError());
    }
    else if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
    }

    // define the variable container
    scilabVar list = scilab_createList(NULL);
    scilab_setVar(L"ModelVariables", list);

    return ret;
}

extern "C" fmi2Status fmi2EnterInitializationMode(fmi2Component _c)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    if (scilab_getVar(L"fmi2EnterInitializationMode"))
    {

        std::string cmd = "fmi2EnterInitializationMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2EnterInitializationMode() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2ExitInitializationMode(fmi2Component _c)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    if (scilab_getVar(L"fmi2ExitInitializationMode"))
    {
        std::string cmd = "fmi2ExitInitializationMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2ExitInitializationMode() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2Terminate(fmi2Component _c)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    if (scilab_getVar(L"fmi2Terminate"))
    {
        std::string cmd = "fmi2Terminate();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2Terminate() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2Reset(fmi2Component _c)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    if (scilab_getVar(L"fmi2Reset"))
    {
        std::string cmd = "fmi2Reset();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2Reset() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2GetReal(fmi2Component _c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
{
    return fmi2GetXXX("fmi2GetReal", 'r', (CallScilabComponent *)_c, vr, nvr, value, scilab_isDouble, scilab_getDouble, scilab_createDouble, report<fmi2Error>);
}

extern "C" fmi2Status fmi2GetInteger(fmi2Component _c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[])
{
    return fmi2GetXXX("fmi2GetInteger", 'i', (CallScilabComponent *)_c, vr, nvr, value, scilab_isInt32, scilab_getInteger32, scilab_createInteger32, report<fmi2Error>);
}

extern "C" fmi2Status fmi2GetBoolean(fmi2Component _c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[])
{
    return fmi2GetXXX("fmi2GetBoolean", 'b', (CallScilabComponent *)_c, vr, nvr, value, scilab_isBoolean, scilab_getBoolean, scilab_createBoolean, report<fmi2Error>);
}

extern "C" fmi2Status fmi2GetString(fmi2Component _c, const fmi2ValueReference vr[], size_t nvr, fmi2String value[])
{
    auto c = (CallScilabComponent *)_c;
    CallScilabComponent::current = c;

    c->returnedString.clear();

    return fmi2GetXXX("fmi2GetString", 's', c, vr, nvr, value, scilab_isString, getString, createString, report<fmi2Error>);
}

extern "C" fmi2Status fmi2SetReal(fmi2Component _c, const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[])
{
    return fmi2SetXXX("fmi2SetReal", 'r', (CallScilabComponent *)_c, vr, nvr, value, scilab_createDouble, report<fmi2Error>);
}

extern "C" fmi2Status fmi2SetInteger(fmi2Component _c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer value[])
{
    return fmi2SetXXX("fmi2SetInteger", 'i', (CallScilabComponent *)_c, vr, nvr, value, scilab_createInteger32, report<fmi2Error>);
}

extern "C" fmi2Status fmi2SetBoolean(fmi2Component _c, const fmi2ValueReference vr[], size_t nvr, const fmi2Boolean value[])
{
    return fmi2SetXXX("fmi2SetBoolean", 'b', (CallScilabComponent *)_c, vr, nvr, value, scilab_createBoolean, report<fmi2Error>);
}

extern "C" fmi2Status fmi2SetString(fmi2Component _c, const fmi2ValueReference vr[], size_t nvr, const fmi2String value[])
{
    return fmi2SetXXX("fmi2SetString", 's', (CallScilabComponent *)_c, vr, nvr, value, createString, report<fmi2Error>);
}

/*
 * These functions are only supported by the FMU, if the optional capability flag
 * <fmiModelDescription> <ModelExchange / CoSimulation canGetAndSetFMUstate in
 * = "true"> in the XML file is explicitly set to true.
 */

extern "C" fmi2Status fmi2GetFMUstate(fmi2Component c, fmi2FMUstate *FMUstate)
{
    return fmi2Discard;
}
extern "C" fmi2Status fmi2SetFMUstate(fmi2Component c, fmi2FMUstate FMUstate)
{
    return fmi2Discard;
}
extern "C" fmi2Status fmi2FreeFMUstate(fmi2Component c, fmi2FMUstate *FMUstate)
{
    return fmi2Discard;
}

/*
 * These functions are only supported by the FMU, if the optional capability flags
 * canGetAndSetFMUstate and canSerializeFMUstate in
 * <fmiModelDescription><ModelExchange / CoSimulation> in the XML file are explicitly
 * set to true
 */

extern "C" fmi2Status fmi2SerializedFMUstateSize(fmi2Component c, fmi2FMUstate FMUstate, size_t *size)
{
    return fmi2Discard;
}
extern "C" fmi2Status fmi2SerializeFMUstate(fmi2Component c, fmi2FMUstate FMUstate, fmi2Byte serializedState[], size_t size)
{
    return fmi2Discard;
}
extern "C" fmi2Status fmi2DeSerializeFMUstate(fmi2Component c, const fmi2Byte serializedState[], size_t size, fmi2FMUstate *FMUstate)
{
    return fmi2Discard;
}

/*
 * If the capability attribute “providesDirectionalDerivative” is true
 */
extern "C" fmi2Status fmi2GetDirectionalDerivative(fmi2Component _c, const fmi2ValueReference vUnknown_ref[], size_t nUnknown, const fmi2ValueReference vKnown_ref[], size_t nKnown, const fmi2Real dvKnown[], fmi2Real dvUnknown[])
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    if (scilab_getVar(L"fmi2GetDirectionalDerivative"))
    {
        scilabVar vUnknown = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nUnknown, 1);
        scilab_setUnsignedInteger32Array(NULL, vUnknown, vUnknown_ref);
        scilab_setVar(L"vUnknown", vUnknown);

        scilabVar vKnown = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nKnown, 1);
        scilab_setUnsignedInteger32Array(NULL, vKnown, vKnown_ref);
        scilab_setVar(L"vKnown", vKnown);

        std::string cmd = "[dvKnown, dvUnknown] = fmi2GetDirectionalDerivative(vUnknown, vKnown);";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetDirectionalDerivative() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
            return ret;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"dvKnown", (double *)dvKnown, nKnown))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetDirectionalDerivative() returned invalid dvKnown, double vector expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"dvUnknown", (double *)dvUnknown, nUnknown))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetDirectionalDerivative() returned invalid dvUnknown, double vector expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
    }
    else
    {
        c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Discard, NULL, "fmi2GetDirectionalDerivative() is not implemented");
        ret = fmi2Discard;
    }

    return ret;
}