#include "fmu_shared.hxx"
#include "fmu3.hxx"

using namespace call_scilab_fmu::fmu3;

extern "C" fmi3Instance fmi3InstantiateModelExchange(
    fmi3String instanceName,
    fmi3String instantiationToken,
    fmi3String resourcePath,
    fmi3Boolean visible,
    fmi3Boolean loggingOn,
    fmi3InstanceEnvironment instanceEnvironment,
    fmi3LogMessageCallback logMessage)
{
    // from call_scilab documentation, the SCI env. variable real impact is unclear (vs LDFLAGS, PATH usage)
    if (Call_ScilabOpen(getenv("SCI"), TRUE, NULL, 0) > 0)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, check your SCI and PATH env. variables.");
        return NULL;
    }

    // the resourcesPath contains the Scilab script
    if (resourcePath == NULL)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, resourcePath expected.");
        return NULL;
    }
    std::filesystem::path resources(resourcePath);
    if (!std::filesystem::is_directory(resources))
    {
        report(logMessage, instanceEnvironment, fmi3Error, "StartScilab() failed, resourcePath \"%s\" is not a directory.", resourcePath);
        return NULL;
    }

    // check and configure the visible flag
    if (ScilabHaveAGraph() == FALSE && visible)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, visible requested but Scilab has been built without GUI.");
        return NULL;
    }
    // visible is not really accessible, disable it and make a Scilab with GUI
    // if (!visible)
    // {
    //     DisableInteractiveMode();
    // }

    // API Scilab context is not handled
    scilabEnv context = NULL;

    CallScilabComponent *c = new CallScilabComponent(instanceName, logMessage, instanceEnvironment, loggingOn, resources, context);

    return c;
}

extern "C" fmi3Status fmi3EnterContinuousTimeMode(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3EnterContinuousTimeMode"))
    {
        std::string cmd = "fmi3EnterContinuousTimeMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3EnterContinuousTimeMode() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }
    return ret;
}

extern "C" fmi3Status fmi3CompletedIntegratorStep(fmi3Instance instance,
                                                  fmi3Boolean noSetFMUStatePriorToCurrentPoint,
                                                  fmi3Boolean *enterEventMode,
                                                  fmi3Boolean *terminateSimulation)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3CompletedIntegratorStep"))
    {
        std::string cmd = "[enterEventMode, terminateSimulation] = fmi3CompletedIntegratorStep(noSetFMUStatePriorToCurrentPoint);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3CompletedIntegratorStep() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (mapBooleanScilabVariable(L"enterEventMode", enterEventMode))
        {
            ret = report<fmi3Error>(c, ret, "fmi3CompletedIntegratorStep() returned an invalid discreteStatesNeedUpdate, boolean expected");
        }
        if (mapBooleanScilabVariable(L"terminateSimulation", terminateSimulation))
        {
            ret = report<fmi3Error>(c, ret, "fmi3CompletedIntegratorStep() returned an invalid terminateSimulation, boolean expected");
        }
    }
    return ret;
}

extern "C" fmi3Status fmi3SetTime(fmi3Instance instance, fmi3Float64 time)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    // always set a variable named time
    scilab_setVar(L"time", scilab_createDouble(NULL, time));

    if (scilab_getVar(L"fmi3SetTime"))
    {
        std::string cmd = "fmi3SetTime(time);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3SetTime() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3SetContinuousStates(fmi3Instance instance,
                                              const fmi3Float64 continuousStates[],
                                              size_t nContinuousStates)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    scilabVar vX = scilab_createDoubleMatrix2d(NULL, (int)nContinuousStates, 1, 0);
    scilab_setDoubleArray(NULL, vX, continuousStates);
    scilab_setVar(L"x", vX);

    if (scilab_getVar(L"fmi3SetContinuousStates"))
    {
        std::string cmd = "fmi3SetContinuousStates(x);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3SetContinuousStates() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetContinuousStateDerivatives(fmi3Instance instance,
                                                        fmi3Float64 derivatives[],
                                                        size_t nContinuousStates)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    std::fill_n(derivatives, nContinuousStates, 0);

    if (scilab_getVar(L"fmi3GetContinuousStateDerivatives"))
    {
        std::string cmd = "[derivatives] = fmi3GetContinuousStateDerivatives();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetContinuousStateDerivatives() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"nextEventTime", derivatives, nContinuousStates))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() returned an invalid derivatives, double vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetEventIndicators(fmi3Instance instance,
                                             fmi3Float64 eventIndicators[],
                                             size_t nEventIndicators)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    std::fill_n(eventIndicators, nEventIndicators, 0);

    if (scilab_getVar(L"fmi3GetEventIndicators"))
    {
        std::string cmd = "[eventIndicators] = fmi3GetEventIndicators();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetEventIndicators() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"nextEventTime", eventIndicators, nEventIndicators))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetEventIndicators() returned an invalid eventIndicators, double vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetContinuousStates(fmi3Instance instance,
                                              fmi3Float64 continuousStates[],
                                              size_t nContinuousStates)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    std::fill_n(continuousStates, nContinuousStates, 0);

    if (scilab_getVar(L"fmi3GetContinuousStates"))
    {
        std::string cmd = "[continuousStates] = fmi3GetContinuousStates();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetContinuousStates() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"continuousStates", continuousStates, nContinuousStates))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetContinuousStates() returned an invalid x, double vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetNominalsOfContinuousStates(fmi3Instance instance,
                                                        fmi3Float64 nominals[],
                                                        size_t nContinuousStates)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    std::fill_n(nominals, nContinuousStates, 0);

    if (scilab_getVar(L"fmi3GetNominalsOfContinuousStates"))
    {
        std::string cmd = "[nominals] = fmi3GetNominalsOfContinuousStates();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetContinuousStates() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"nominals", nominals, nContinuousStates))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetContinuousStates() returned an invalid x, double vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetNumberOfEventIndicators(fmi3Instance instance,
                                                     size_t *nEventIndicators)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetNumberOfEventIndicators"))
    {
        std::string cmd = "[nEventIndicators] = fmi3GetNumberOfEventIndicators();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetNumberOfEventIndicators() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (mapSizeTScilabVariable(L"nEventIndicators", nEventIndicators))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() returned an invalid nEventIndicators, scalar double expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetNumberOfContinuousStates(fmi3Instance instance,
                                                      size_t *nContinuousStates)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetNumberOfContinuousStates"))
    {
        std::string cmd = "[nContinuousStates] = fmi3GetNumberOfContinuousStates();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetNumberOfContinuousStates() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (mapSizeTScilabVariable(L"nContinuousStates", nContinuousStates))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetNumberOfContinuousStates() returned an invalid nContinuousStates, scalar double expected");
        }
    }

    return ret;
}