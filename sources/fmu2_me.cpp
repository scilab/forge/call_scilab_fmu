extern "C"
{
#include "fmi2TypesPlatform.h"
#include "fmi2Functions.h"

#include <call_scilab.h>
#include <api_scilab.h>
#include <charEncoding.h>
}

#include <algorithm>
#include <string>
#include "fmu_shared.hxx"
#include "fmu2.hxx"

using namespace call_scilab_fmu::fmu2;

namespace
{

} /* namespace */

/***************************************************
Functions for FMI2 for Model Exchange
****************************************************/

extern "C" fmi2Status fmi2EnterEventMode(fmi2Component _c)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    if (scilab_getVar(L"fmi2EnterEventMode"))
    {
        std::string cmd = "fmi2EnterEventMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2EnterEventMode() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2NewDiscreteStates(fmi2Component _c, fmi2EventInfo *fmi2eventInfo)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    // by default, set them all to 0
    *fmi2eventInfo = {};

    if (scilab_getVar(L"fmi2NewDiscreteStates"))
    {
        std::string cmd = "[newDiscreteStatesNeeded, terminateSimulation, nominalsOfContinuousStatesChanged, valuesOfContinuousStatesChanged, nextEventTimeDefined, nextEventTime] = fmi2NewDiscreteStates();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2NewDiscreteStates() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
            return ret;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapScalarScilabVariable(L"newDiscreteStatesNeeded", scilab_isBoolean, scilab_getBoolean, &fmi2eventInfo->newDiscreteStatesNeeded))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2NewDiscreteStates() returned an invalid newDiscreteStatesNeeded, boolean expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }

        if (call_scilab_fmu::mapScalarScilabVariable(L"terminateSimulation", scilab_isBoolean, scilab_getBoolean, &fmi2eventInfo->terminateSimulation))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2NewDiscreteStates() returned an invalid terminateSimulation, boolean expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }

        if (call_scilab_fmu::mapScalarScilabVariable(L"nominalsOfContinuousStatesChanged", scilab_isBoolean, scilab_getBoolean, &fmi2eventInfo->nominalsOfContinuousStatesChanged))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2NewDiscreteStates() returned an invalid nominalsOfContinuousStatesChanged, boolean expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }

        if (call_scilab_fmu::mapScalarScilabVariable(L"valuesOfContinuousStatesChanged", scilab_isBoolean, scilab_getBoolean, &fmi2eventInfo->valuesOfContinuousStatesChanged))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2NewDiscreteStates() returned an invalid valuesOfContinuousStatesChanged, boolean expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }

        if (call_scilab_fmu::mapScalarScilabVariable(L"nextEventTimeDefined", scilab_isBoolean, scilab_getBoolean, &fmi2eventInfo->nextEventTimeDefined))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2NewDiscreteStates() returned an invalid nextEventTimeDefined, boolean expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }

        if (call_scilab_fmu::mapScalarScilabVariable(L"nextEventTime", scilab_isDouble, scilab_getDouble, &fmi2eventInfo->nextEventTime))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2NewDiscreteStates() returned an invalid nextEventTime, double expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
    }

    return ret;
}

extern "C" fmi2Status fmi2EnterContinuousTimeMode(fmi2Component _c)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    if (scilab_getVar(L"fmi2EnterContinuousTimeMode"))
    {

        std::string cmd = "fmi2EnterContinuousTimeMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2EnterContinuousTimeMode() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2CompletedIntegratorStep(fmi2Component c,
                                                  fmi2Boolean noSetFMUStatePriorToCurrentPoint,
                                                  fmi2Boolean *enterEventMode,
                                                  fmi2Boolean *terminateSimulation)
{
    // this is not needed as completedIntegratorStepNotNeeded = false
    *enterEventMode = fmi2False;
    *terminateSimulation = fmi2False;
    return fmi2OK;
}

extern "C" fmi2Status fmi2SetTime(fmi2Component _c, fmi2Real time)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    // always set a variable named time
    scilab_setVar(L"time", scilab_createDouble(NULL, time));

    if (scilab_getVar(L"fmi2SetTime"))
    {
        std::string cmd = "fmi2SetTime(time);";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2SetTime() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2SetContinuousStates(fmi2Component _c, const fmi2Real x[], size_t nx)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    scilabVar varX = scilab_createDoubleMatrix2d(NULL, (int)nx, 1, 0);
    scilab_setDoubleArray(NULL, varX, x);
    scilab_setVar(L"x", varX);

    if (scilab_getVar(L"fmi2SetContinuousStates"))
    {
        std::string cmd = "fmi2SetContinuousStates(x);";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2SetTime() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }
    }

    return fmi2OK;
}

extern "C" fmi2Status fmi2GetDerivatives(fmi2Component _c, fmi2Real derivatives[], size_t nx)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    // by default, set them all to 0
    std::fill_n(derivatives, nx, 0);

    if (scilab_getVar(L"fmi2GetDerivatives"))
    {
        std::string cmd = "derivatives = fmi2GetDerivatives();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetDerivatives() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"derivatives", derivatives, nx))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetDerivatives() returned invalid derivatives, double vector expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
    }

    return ret;
}

extern "C" fmi2Status fmi2GetEventIndicators(fmi2Component _c, fmi2Real eventIndicators[], size_t ni)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    // by default, set them all to 0
    std::fill_n(eventIndicators, ni, 0);

    if (scilab_getVar(L"fmi2GetEventIndicators"))
    {
        std::string cmd = "eventIndicators = fmi2GetEventIndicators();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetEventIndicators() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"eventIndicators", eventIndicators, ni))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetEventIndicators() returned invalid eventIndicators, double vector expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
    }

    return ret;
}

extern "C" fmi2Status fmi2GetContinuousStates(fmi2Component _c, fmi2Real x[], size_t nx)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    // by default, set them all to 0
    std::fill_n(x, nx, 0);

    if (scilab_getVar(L"fmi2GetContinuousStates"))
    {
        std::string cmd = "x = fmi2GetContinuousStates();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetContinuousStates() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"x", x, nx))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetContinuousStates() returned invalid x, double vector expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
    }

    return ret;
}

extern "C" fmi2Status fmi2GetNominalsOfContinuousStates(fmi2Component _c, fmi2Real x_nominal[], size_t nx)
{
    auto c = (CallScilabComponent *)_c;
    fmi2Status ret = fmi2OK;

    // by default, set them all to 0
    std::fill_n(x_nominal, nx, 0);

    if (scilab_getVar(L"fmi2GetNominalsOfContinuousStates"))
    {
        std::string cmd = "x_nominal = fmi2GetNominalsOfContinuousStates();";
        if (c->ExecuteScilabCommand(cmd))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetNominalsOfContinuousStates() failed to execute.\n%s", c->formatError());
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
        else if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"x_nominal", x_nominal, nx))
        {
            c->logger(c->componentEnvironment, (fmi2String)c->instanceName.c_str(), fmi2Error, NULL, "fmi2GetNominalsOfContinuousStates() returned invalid x_nominal, double vector expected.");
            if (ret < fmi2Error)
                ret = fmi2Error;
        }
    }

    return ret;
}
