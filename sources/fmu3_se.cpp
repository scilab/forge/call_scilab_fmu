#include "fmu_shared.hxx"
#include "fmu3.hxx"

using namespace call_scilab_fmu::fmu3;

extern "C" fmi3Instance fmi3InstantiateScheduledExecution(
    fmi3String                     instanceName,
    fmi3String                     instantiationToken,
    fmi3String                     resourcePath,
    fmi3Boolean                    visible,
    fmi3Boolean                    loggingOn,
    fmi3InstanceEnvironment        instanceEnvironment,
    fmi3LogMessageCallback         logMessage,
    fmi3ClockUpdateCallback        clockUpdate,
    fmi3LockPreemptionCallback     lockPreemption,
    fmi3UnlockPreemptionCallback   unlockPreemption)
{
    // from call_scilab documentation, the SCI env. variable real impact is unclear (vs LDFLAGS, PATH usage)
    if (Call_ScilabOpen(getenv("SCI"), TRUE, NULL, 0) > 0)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, check your SCI and PATH env. variables.");
        return NULL;
    }

    // the resourcesPath contains the Scilab script
    if (resourcePath == NULL)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, resourcePath expected.");
        return NULL;
    }
    std::filesystem::path resources(resourcePath);
    if (!std::filesystem::is_directory(resources))
    {
        report(logMessage, instanceEnvironment, fmi3Error, "StartScilab() failed, resourcePath \"%s\" is not a directory.", resourcePath);
        return NULL;
    }

    // check and configure the visible flag
    if (ScilabHaveAGraph() == FALSE && visible)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, visible requested but Scilab has been built without GUI.");
        return NULL;
    }
    // visible is not really accessible, disable it and make a Scilab with GUI
    // if (!visible)
    // {
    //     DisableInteractiveMode();
    // }

    // API Scilab context is not handled
    scilabEnv context = NULL;
    
    CallScilabComponent *c = new CallScilabComponent(instanceName, logMessage, instanceEnvironment, loggingOn, resources, context);

    return c;
}

extern "C" fmi3Status fmi3ActivateModelPartition(fmi3Instance instance,
                                                  fmi3ValueReference clockReference,
                                                  fmi3Float64 activationTime)
{
    auto c = (CallScilabComponent *) instance;
    fmi3Status ret = fmi3OK;

    scilab_setVar(L"clockReference", scilab_createUnsignedInteger32(NULL, clockReference));
    scilab_setVar(L"activationTime", scilab_createDouble(NULL, activationTime));

    std::string cmd = "ModelVariables = fmi3ActivateModelPartition(ModelVariables);";
    if (c->ExecuteScilabCommand(cmd))
    {
        ret = report<fmi3Error>(c, ret, "fmi3ActivateModelPartition() failed to execute.\n%s", c->formatError());
        return ret;
    }
    if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
    }

    return ret;
}