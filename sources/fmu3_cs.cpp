#include "fmu_shared.hxx"
#include "fmu3.hxx"

using namespace call_scilab_fmu::fmu3;

extern "C" fmi3Instance fmi3InstantiateCoSimulation(
    fmi3String instanceName,
    fmi3String instantiationToken,
    fmi3String resourcePath,
    fmi3Boolean visible,
    fmi3Boolean loggingOn,
    fmi3Boolean eventModeUsed,
    fmi3Boolean earlyReturnAllowed,
    const fmi3ValueReference requiredIntermediateVariables[],
    size_t nRequiredIntermediateVariables,
    fmi3InstanceEnvironment instanceEnvironment,
    fmi3LogMessageCallback logMessage,
    fmi3IntermediateUpdateCallback intermediateUpdate)
{
    // from call_scilab documentation, the SCI env. variable real impact is unclear (vs LDFLAGS, PATH usage)
    if (Call_ScilabOpen(getenv("SCI"), TRUE, NULL, 0) > 0)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, check your SCI and PATH env. variables.");
        return NULL;
    }

    // the resourcesPath contains the Scilab script
    if (resourcePath == NULL)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, resourcePath expected.");
        return NULL;
    }
    std::filesystem::path resources(resourcePath);
    if (!std::filesystem::is_directory(resources))
    {
        report(logMessage, instanceEnvironment, fmi3Error, "StartScilab() failed, resourcePath \"%s\" is not a directory.", resourcePath);
        return NULL;
    }

    // check and configure the visible flag
    if (ScilabHaveAGraph() == FALSE && visible)
    {
        logMessage(instanceEnvironment, fmi3Error, NULL, "StartScilab() failed, visible requested but Scilab has been built without GUI.");
        return NULL;
    }
    // visible is not really accessible, disable it and make a Scilab with GUI
    // if (!visible)
    // {
    //     DisableInteractiveMode();
    // }

    // API Scilab context is not handled
    scilabEnv context = NULL;

    CallScilabComponent *c = new CallScilabComponent(instanceName, logMessage, instanceEnvironment, loggingOn, resources, context);

    return c;
}

extern "C" fmi3Status fmi3EnterStepMode(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3EnterStepMode"))
    {
        std::string cmd = "fmi3EnterStepMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3EnterStepMode() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetOutputDerivatives(fmi3Instance instance,
                                               const fmi3ValueReference valueReferences[],
                                               size_t nValueReferences,
                                               const fmi3Int32 orders[],
                                               fmi3Float64 values[],
                                               size_t nValues)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetOutputDerivatives"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        scilabVar vOrders = scilab_createInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setInteger32Array(NULL, vOrders, orders);
        scilab_setVar(L"orders", vOrders);

        scilab_setVar(L"nValues", scilab_createDouble(NULL, (int)nValues));

        std::string cmd = "[values] = fmi3GetOutputDerivatives(valueReferences, orders, nValues);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetOutputDerivatives() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"values", values, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetOutputDerivatives() return invalid values, double vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3DoStep(fmi3Instance instance,
                                 fmi3Float64 currentCommunicationPoint,
                                 fmi3Float64 communicationStepSize,
                                 fmi3Boolean noSetFMUStatePriorToCurrentPoint,
                                 fmi3Boolean *eventHandlingNeeded,
                                 fmi3Boolean *terminateSimulation,
                                 fmi3Boolean *earlyReturn,
                                 fmi3Float64 *lastSuccessfulTime)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    scilab_setVar(L"currentCommunicationPoint", scilab_createDouble(NULL, currentCommunicationPoint));
    scilab_setVar(L"communicationStepSize", scilab_createDouble(NULL, communicationStepSize));
    scilab_setVar(L"noSetFMUStatePriorToCurrentPoint", scilab_createBoolean(NULL, noSetFMUStatePriorToCurrentPoint));

    std::string cmd = "[ModelVariables] = fmi3DoStep(ModelVariables);";
    if (c->ExecuteScilabCommand(cmd))
    {
        ret = report<fmi3Error>(c, ret, "fmi3DoStep() failed to execute.\n%s", c->formatError());
        return ret;
    }
    if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
    }

    scilabVar var = scilab_getVar(L"eventHandlingNeeded");
    if (var)
    {
        if (mapBooleanScilabVariable(L"eventHandlingNeeded", eventHandlingNeeded))
        {
            ret = report<fmi3Error>(c, ret, "fmi3DoStep() returned an invalid eventHandlingNeeded, boolean expected");
        }
        scilab_setBoolean(NULL, var, 0);
    }
    else
    {
        *eventHandlingNeeded = false;
    }

    var = scilab_getVar(L"terminateSimulation");
    if (var)
    {
        if (mapBooleanScilabVariable(L"terminateSimulation", terminateSimulation))
        {
            ret = report<fmi3Error>(c, ret, "fmi3DoStep() returned an invalid terminateSimulation, boolean expected");
        }
        scilab_setBoolean(NULL, var, 0);
    }
    else
    {
        *terminateSimulation = false;
    }

    var = scilab_getVar(L"earlyReturn");
    if (var)
    {
        if (mapBooleanScilabVariable(L"earlyReturn", earlyReturn))
        {
            ret = report<fmi3Error>(c, ret, "fmi3DoStep() returned an invalid earlyReturn, boolean expected");
        }
        scilab_setBoolean(NULL, var, 0);
    }
    else
    {
        *earlyReturn = false;
    }

    var = scilab_getVar(L"lastSuccessfulTime");
    if (var)
    {
        if (call_scilab_fmu::mapScalarScilabVariable<double>(L"lastSuccessfulTime", scilab_isDouble, scilab_getDouble, lastSuccessfulTime))
        {
            ret = report<fmi3Error>(c, ret, "fmi3DoStep() returned an invalid lastSuccessfulTime, double expected");
        }
        scilab_setDoubleArray(NULL, var, &currentCommunicationPoint);
    }
    else
    {
        *lastSuccessfulTime = currentCommunicationPoint;
    }

    return ret;
}
