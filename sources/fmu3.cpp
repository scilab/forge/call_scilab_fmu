extern "C"
{
#include "fmi3PlatformTypes.h"
#include "fmi3Functions.h"

#include <call_scilab.h>
#include <api_scilab.h>
#include <charEncoding.h>
#include <version.h>
}

#include <memory>
#include <string>
#include <vector>
#include <filesystem>

#include "fmu_shared.hxx"
#include "fmu3.hxx"

using namespace call_scilab_fmu::fmu3;

namespace
{
    scilabStatus getStringArray(scilabEnv env, scilabVar var, fmi3String **vals)
    {
        int len = scilab_getSize(env, var);

        wchar_t **returned;
        if (scilab_getStringArray(env, var, &returned))
        {
            return STATUS_ERROR;
        }

        size_t startAt = CallScilabComponent::current->returnedString.size();
        for (int i = 0; i < len; ++i)
        {
            CallScilabComponent::current->returnedString.emplace_back(returned[i]);
        }
        // current->returnedString storage is compatible with std::vector<char*>, pass the pointer around !
        static_assert(sizeof(call_scilab_fmu::owned_string_t) == sizeof(char *));
        *vals = (fmi3String *)CallScilabComponent::current->returnedString.data() + startAt;
        return STATUS_OK;
    }

    scilabStatus setStringArray(scilabEnv env, scilabVar var, const fmi3String vals[])
    {
        int len = scilab_getSize(env, var);

        std::vector<wchar_t *> temp;
        for (int i = 0; i < len; ++i)
        {
            temp.emplace_back(to_wide_string(vals[i]));
        }

        scilabStatus ret = scilab_setStringArray(env, var, temp.data());
        for (int i = 0; i < len; ++i)
        {
            FREE(temp[i]);
        }

        return ret;
    }

    scilabVar createString(scilabEnv env, fmi3String v)
    {
        static std::unique_ptr<wchar_t> temp;

        temp = std::unique_ptr<wchar_t>(to_wide_string(v));
        return scilab_createString(env, temp.get());
    }

    // helper function template for getting a value
    template <typename T, int isT(scilabEnv, scilabVar), scilabStatus getTArray(scilabEnv, scilabVar, T **), scilabVar createT(scilabEnv, T), fmi3Status reportError(CallScilabComponent *, fmi3Status, const char *, ...)>
    fmi3Status fmi3GetXXX(const std::string &fname, CallScilabComponent *c, const unsigned int vr[], size_t nvr, T values[], size_t nValues)
    {
        fmi3Status ret = fmi3OK;

        scilabVar list = scilab_getVar(L"ModelVariables");
        if (list == NULL)
        {
            ret = reportError(c, ret, "%s() failed, ModelVariables does not exist.", fname.c_str());
            return ret;
        }

        size_t consumed = 0;
        for (size_t i = 0; i < nvr; ++i)
        {
            // 1-indexed for Scilab internal list
            if (vr[i] < 1)
            {
                ret = reportError(c, ret, "%s() failed, #%d# valueReference should be 1 or more.", fname.c_str(), vr[i]);
                continue;
            }
            if (vr[i] > (size_t) std::numeric_limits<int>::max())
            {
                ret = reportError(c, ret, "%s() failed, #%d# valueReference less than %d.", fname.c_str(), vr[i], std::numeric_limits<int>::max());
                continue;
            }
            int index = (int) (vr[i] - 1);

            if (index < scilab_getSize(NULL, list) && scilab_isDefined(NULL, list, index))
            {
                scilabVar v = scilab_getListItem(NULL, list, index);

                int len = scilab_getSize(NULL, v);
                if (consumed + len > nValues)
                {
                    ret = reportError(c, ret, "%s() failed, #%d# ModelVariables(%d) has %d values to store.", fname.c_str(), vr[i], vr[i] + 1, len);
                    continue;
                }
                if (!isT(NULL, v))
                {
                    ret = reportError(c, ret, "%s() failed, #%d# ModelVariables(%d) type is not %s.", fname.c_str(), vr[i], vr[i] + 1, typeid(T).name());
                    continue;
                }
                T *returned;
                if (getTArray(NULL, v, &returned))
                {
                    ret = reportError(c, ret, "%s() failed, #%d# ModelVariables(%d) cannot be read.", fname.c_str(), vr[i], vr[i] + 1);
                    continue;
                }
                for (int j = 0; j < len; ++j)
                {
                    values[consumed++] = returned[i];
                }
            }
            else
            {
                T v{};
                // set default value on Scilab-side if it does not exist yet
                scilab_setListItem(NULL, list, index, createT(NULL, v));
                values[i] = v;
            }
        }
        return ret;
    }

    scilabVar createDoubleMatrix2d(scilabEnv env, int row, int col)
    {
        return scilab_createDoubleMatrix2d(env, row, col, 0);
    }

    // helper function template for setting a value
    template <typename T, scilabVar createTMatrix2d(scilabEnv, int, int), scilabStatus setTArray(scilabEnv, scilabVar, const T *), fmi3Status reportError(CallScilabComponent *, fmi3Status, const char *, ...)>
    fmi3Status fmi3SetXXX(const std::string &fname, CallScilabComponent *c, const unsigned int vr[], size_t nvr, const T values[], size_t nValues)
    {
        fmi3Status ret = fmi3OK;

        scilabVar list = scilab_getVar(L"ModelVariables");
        if (list == NULL)
        {
            ret = reportError(c, ret, "%s() failed, ModelVariables does not exist.", fname.c_str());
            return ret;
        }

        for (size_t i = 0; i < nvr; ++i)
        {
            // 1-indexed for Scilab internal list
            if (vr[i] < 1)
            {
                ret = reportError(c, ret, "%s() failed, #%d# valueReference should be 1 or more.", fname.c_str(), vr[i]);
                continue;
            }
            if (vr[i] > (size_t) std::numeric_limits<int>::max())
            {
                ret = reportError(c, ret, "%s() failed, #%d# valueReference less than %d.", fname.c_str(), vr[i], std::numeric_limits<int>::max());
                continue;
            }
            int index = (int) (vr[i] - 1);

            // make the last value reference consume all the remaining values
            size_t len = 1;
            if (i == nvr - 1)
            {
                len = nValues - i;
            }
            scilabVar v = createTMatrix2d(NULL, (int)len, 1);
            if (v == NULL)
            {
                ret = reportError(c, ret, "%s() failed, unable to create #%d# ModelVariables(%d).", fname.c_str(), vr[i], vr[i] + 1);
                continue;
            }

            if (setTArray(NULL, v, &values[i]))
            {
                ret = reportError(c, ret, "%s() failed, unable to assign #%d# ModelVariables(%d).", fname.c_str(), vr[i], vr[i] + 1);
                continue;
            }

            // switch to 0-indexed for Scilab internal list
            if (scilab_setListItem(NULL, list, index, v))
            {
                ret = reportError(c, ret, "%s() failed, unable to set #%d# ModelVariables(%d).", fname.c_str(), vr[i], vr[i] + 1);
                continue;
            }
        }
        return ret;
    }

} /* namespace */

extern "C" const char *fmi3GetVersion(void)
{
    return "3.0";
}

extern "C" fmi3Status fmi3SetDebugLogging(fmi3Instance instance,
                                          fmi3Boolean loggingOn,
                                          size_t nCategories,
                                          const fmi3String categories[])
{
    auto c = (CallScilabComponent *)instance;
    c->loggingOn = loggingOn;

    if (nCategories > 1)
    {
        c->logger(c->componentEnvironment, fmi3Warning, NULL, "fmi3SetDebugLogging() failed, LogCategories are not supported.");
        return fmi3Warning;
    }

    return fmi3OK;
}

extern "C" void fmi3FreeInstance(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    TerminateScilab(NULL);
    delete c;
}

extern "C" fmi3Status fmi3EnterInitializationMode(fmi3Instance instance,
                                                  fmi3Boolean toleranceDefined,
                                                  fmi3Float64 tolerance,
                                                  fmi3Float64 startTime,
                                                  fmi3Boolean stopTimeDefined,
                                                  fmi3Float64 stopTime)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    // setup arguments as Scilab variables
    if (toleranceDefined)
    {
        scilab_setVar(L"tolerance", scilab_createDouble(NULL, tolerance));
    }
    scilab_setVar(L"startTime", scilab_createDouble(NULL, startTime));
    if (stopTimeDefined)
    {
        scilab_setVar(L"stopTime", scilab_createDouble(NULL, stopTime));
    }

    // load all macros on resources/
    std::string cmd;
    cmd += "getd(\"";
    cmd += c->resourcesPath.string();
    cmd += "\");";
    if (c->ExecuteScilabCommand(cmd))
    {
        ret = report<fmi3Error>(c, ret, "fmi3EnterInitializationMode() failed, resources directory \"%s\" cannot be loaded.\n%s", c->resourcesPath.c_str(), c->formatError());
        return ret;
    }
    if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
    }

    // load all macros on CWD, this is very usefull to debug and is an acceptable error
    auto p = std::filesystem::current_path().string();
    cmd = "getd(\"" + p + "\");";
    if (c->ExecuteScilabCommand(cmd))
    {
        ret = report<fmi3OK>(c, ret, "fmi3EnterInitializationMode() warned, current directory \"%s\" cannot be loaded.\n%s", p.c_str(), c->formatError());
    }
    else if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
    }

    // execute all scripts on resources/
    cmd = "exec(ls(\"" + p + "/*.sce\"));";
    if (c->ExecuteScilabCommand(cmd))
    {
        ret = report<fmi3OK>(c, ret, "fmi3EnterInitializationMode() warned, current directory \"%s\" cannot be loaded.\n%s", p.c_str(), c->formatError());
    }
    else if (c->loggingOn)
    {
        c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
    }

    // define the variable container
    scilabVar list = scilab_createList(NULL);
    scilab_setVar(L"ModelVariables", list);

    // call the Scilab implementation
    if (scilab_getVar(L"fmi3EnterInitializationMode"))
    {
        cmd = "fmi3EnterInitializationMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi2EnterInitializationMode() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3ExitInitializationMode(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3ExitInitializationMode"))
    {
        std::string cmd = "fmi3ExitInitializationMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3ExitInitializationMode() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3EnterEventMode(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3EnterEventMode"))
    {
        std::string cmd = "fmi3EnterEventMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3EnterEventMode() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3Terminate(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3Terminate"))
    {
        std::string cmd = "fmi3Terminate();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3Terminate() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3Reset(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3Reset"))
    {
        std::string cmd = "fmi3Reset();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3Reset() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetFloat32(fmi3Instance instance,
                                     const fmi3ValueReference valueReferences[],
                                     size_t nValueReferences,
                                     fmi3Float32 values[],
                                     size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3Float32, scilab_isDouble, getTArray<fmi3Float32, double, scilab_getDoubleArray>, createT<fmi3Float32, double, scilab_createDouble>, report<fmi3Error>>;
    return f("fmi3GetFloat32", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetFloat64(fmi3Instance instance,
                                     const fmi3ValueReference valueReferences[],
                                     size_t nValueReferences,
                                     fmi3Float64 values[],
                                     size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3Float64, scilab_isDouble, scilab_getDoubleArray, scilab_createDouble, report<fmi3Error>>;
    return f("fmi3GetFloat64", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetInt8(fmi3Instance instance,
                                  const fmi3ValueReference valueReferences[],
                                  size_t nValueReferences,
                                  fmi3Int8 values[],
                                  size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3Int8, scilab_isInt8, getCastTArray<fmi3Int8, char, scilab_getInteger8Array>, createT<fmi3Int8, char, scilab_createInteger8>, report<fmi3Error>>;
    return f("fmi3GetInt8", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetUInt8(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   fmi3UInt8 values[],
                                   size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3UInt8, scilab_isUnsignedInt8, scilab_getUnsignedInteger8Array, scilab_createUnsignedInteger8, report<fmi3Error>>;
    return f("fmi3GetUInt8", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetInt16(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   fmi3Int16 values[],
                                   size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3Int16, scilab_isInt16, scilab_getInteger16Array, scilab_createInteger16, report<fmi3Error>>;
    return f("fmi3GetInt16", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetUInt16(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    fmi3UInt16 values[],
                                    size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3UInt16, scilab_isUnsignedInt16, scilab_getUnsignedInteger16Array, scilab_createUnsignedInteger16, report<fmi3Error>>;
    return f("fmi3GetUInt16", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetInt32(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   fmi3Int32 values[],
                                   size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3Int32, scilab_isInt32, scilab_getInteger32Array, scilab_createInteger32, report<fmi3Error>>;
    return f("fmi3GetInt32", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetUInt32(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    fmi3UInt32 values[],
                                    size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3UInt32, scilab_isUnsignedInt32, scilab_getUnsignedInteger32Array, scilab_createUnsignedInteger32, report<fmi3Error>>;
    return f("fmi3GetUInt32", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetInt64(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   fmi3Int64 values[],
                                   size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3Int64, scilab_isInt64, getCastTArray<fmi3Int64, signed long long, scilab_getInteger64Array>, createT<fmi3Int64, signed long long, scilab_createInteger64>, report<fmi3Error>>;
    return f("fmi3GetInt64", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetUInt64(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    fmi3UInt64 values[],
                                    size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3UInt64, scilab_isUnsignedInt64, getCastTArray<fmi3UInt64, unsigned long long, scilab_getUnsignedInteger64Array>, createT<fmi3UInt64, unsigned long long, scilab_createUnsignedInteger64>, report<fmi3Error>>;
    return f("fmi3GetUInt64", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetBoolean(fmi3Instance instance,
                                     const fmi3ValueReference valueReferences[],
                                     size_t nValueReferences,
                                     fmi3Boolean values[],
                                     size_t nValues)
{
    auto const f = fmi3GetXXX<fmi3Boolean, scilab_isBoolean, getTArray<fmi3Boolean, int, scilab_getBooleanArray>, createT<fmi3Boolean, int, scilab_createBoolean>, report<fmi3Error>>;
    return f("fmi3GetBoolean", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetString(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    fmi3String values[],
                                    size_t nValues)
{
    auto c = (CallScilabComponent *)instance;
    CallScilabComponent::current = c;

    c->returnedString.clear();

    auto const f = fmi3GetXXX<fmi3String, scilab_isString, getStringArray, createString, report<fmi3Error>>;
    return f("fmi3GetString", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValues);
}

extern "C" fmi3Status fmi3GetBinary(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    size_t valueSizes[],
                                    fmi3Binary values[],
                                    size_t nValues)
{
    // this is not implemented, Scilab has no binary datatype
    auto c = (CallScilabComponent *)instance;
    c->logger(c->componentEnvironment, fmi3Discard, NULL, "fmi3GetBinary() is not implemented");
    return fmi3Error;
}

extern "C" fmi3Status fmi3GetClock(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   fmi3Clock values[])
{
    auto const f = fmi3GetXXX<fmi3Clock, scilab_isBoolean, getTArray<fmi3Boolean, int, scilab_getBooleanArray>, createT<fmi3Boolean, int, scilab_createBoolean>, report<fmi3Error>>;
    return f("fmi3GetClock", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetFloat32(fmi3Instance instance,
                                     const fmi3ValueReference valueReferences[],
                                     size_t nValueReferences,
                                     const fmi3Float32 values[],
                                     size_t nValues)
{
    auto const f = fmi3SetXXX<fmi3Float32, createDoubleMatrix2d, setTArray<fmi3Float32, double, scilab_setDoubleArray>, report<fmi3Error>>;
    return f("fmi3SetFloat32", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetFloat64(fmi3Instance instance,
                                     const fmi3ValueReference valueReferences[],
                                     size_t nValueReferences,
                                     const fmi3Float64 values[],
                                     size_t nValues)
{
    auto const f = fmi3SetXXX<fmi3Float64, createDoubleMatrix2d, scilab_setDoubleArray, report<fmi3Error>>;
    return f("fmi3SetFloat64", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetInt8(fmi3Instance instance,
                                  const fmi3ValueReference valueReferences[],
                                  size_t nValueReferences,
                                  const fmi3Int8 values[],
                                  size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3Int8, scilab_createInteger8Matrix2d, setCastTArray<fmi3Int8, char, scilab_setInteger8Array>, report<fmi3Error>>;
    return f("fmi3SetInt8", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetUInt8(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   const fmi3UInt8 values[],
                                   size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3UInt8, scilab_createUnsignedInteger8Matrix2d, scilab_setUnsignedInteger8Array, report<fmi3Error>>;
    return f("fmi3SetUInt8", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetInt16(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   const fmi3Int16 values[],
                                   size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3Int16, scilab_createInteger16Matrix2d, scilab_setInteger16Array, report<fmi3Error>>;
    return f("fmi3SetInt16", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetUInt16(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    const fmi3UInt16 values[],
                                    size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3UInt16, scilab_createUnsignedInteger16Matrix2d, scilab_setUnsignedInteger16Array, report<fmi3Error>>;
    return f("fmi3SetUInt16", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetInt32(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   const fmi3Int32 values[],
                                   size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3Int32, scilab_createInteger32Matrix2d, scilab_setInteger32Array, report<fmi3Error>>;
    return f("fmi3SetInt32", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetUInt32(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    const fmi3UInt32 values[],
                                    size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3UInt32, scilab_createUnsignedInteger32Matrix2d, scilab_setUnsignedInteger32Array, report<fmi3Error>>;
    return f("fmi3SetUInt32", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetInt64(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   const fmi3Int64 values[],
                                   size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3Int64, scilab_createInteger64Matrix2d, setTArray<fmi3Int64, long long, scilab_setInteger64Array>, report<fmi3Error>>;
    return f("fmi3SetInt64", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetUInt64(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    const fmi3UInt64 values[],
                                    size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3UInt64, scilab_createUnsignedInteger64Matrix2d, setTArray<fmi3UInt64, unsigned long long, scilab_setUnsignedInteger64Array>, report<fmi3Error>>;
    return f("fmi3SetUInt64", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetBoolean(fmi3Instance instance,
                                     const fmi3ValueReference valueReferences[],
                                     size_t nValueReferences,
                                     const fmi3Boolean values[],
                                     size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3Boolean, scilab_createBooleanMatrix2d, setTArray<fmi3Boolean, int, scilab_setBooleanArray>, report<fmi3Error>>;
    return f("fmi3SetBoolean", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetString(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    const fmi3String values[],
                                    size_t nValues)
{
    const auto f = fmi3SetXXX<fmi3String, scilab_createStringMatrix2d, setStringArray, report<fmi3Error>>;
    return f("fmi3SetString", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3SetBinary(fmi3Instance instance,
                                    const fmi3ValueReference valueReferences[],
                                    size_t nValueReferences,
                                    const size_t valueSizes[],
                                    const fmi3Binary values[],
                                    size_t nValues)
{
    // this is not implemented, Scilab has no binary datatype
    auto c = (CallScilabComponent *)instance;
    c->logger(c->componentEnvironment, fmi3Discard, NULL, "fmi3SetBinary() is not implemented");
    return fmi3Error;
}

extern "C" fmi3Status fmi3SetClock(fmi3Instance instance,
                                   const fmi3ValueReference valueReferences[],
                                   size_t nValueReferences,
                                   const fmi3Clock values[])
{
    const auto f = fmi3SetXXX<fmi3Clock, scilab_createBooleanMatrix2d, setTArray<fmi3Clock, int, scilab_setBooleanArray>, report<fmi3Error>>;
    return f("fmi3SetClock", (CallScilabComponent *)instance, valueReferences, nValueReferences, values, nValueReferences);
}

extern "C" fmi3Status fmi3GetNumberOfVariableDependencies(fmi3Instance instance,
                                                          fmi3ValueReference valueReference,
                                                          size_t *nDependencies)
{
    *nDependencies = 0;
    return fmi3OK;
}

extern "C" fmi3Status fmi3GetVariableDependencies(fmi3Instance instance,
                                                  fmi3ValueReference dependent,
                                                  size_t elementIndicesOfDependent[],
                                                  fmi3ValueReference independents[],
                                                  size_t elementIndicesOfIndependents[],
                                                  fmi3DependencyKind dependencyKinds[],
                                                  size_t nDependencies)
{
    auto c = (CallScilabComponent *)instance;

    if (nDependencies)
    {
        c->logger(c->componentEnvironment, fmi3Discard, NULL, "fmi3GetVariableDependencies() is not implemented");
        return fmi3Discard;
    }
    return fmi3OK;
}

/*
 * Using void* FMUState is not supported
 * TODO: evaluate FMUState usage and implement using a specific Scilab variable name
 */

extern "C" fmi3Status fmi3GetFMUState(fmi3Instance instance, fmi3FMUState *FMUState)
{
    return fmi3Discard;
}

extern "C" fmi3Status fmi3SetFMUState(fmi3Instance instance, fmi3FMUState FMUState)
{
    return fmi3Discard;
}

extern "C" fmi3Status fmi3FreeFMUState(fmi3Instance instance, fmi3FMUState *FMUState)
{
    return fmi3Discard;
}

extern "C" fmi3Status fmi3SerializedFMUStateSize(fmi3Instance instance,
                                                 fmi3FMUState FMUState,
                                                 size_t *size)
{
    return fmi3Discard;
}

extern "C" fmi3Status fmi3SerializeFMUState(fmi3Instance instance,
                                            fmi3FMUState FMUState,
                                            fmi3Byte serializedState[],
                                            size_t size)
{
    return fmi3Discard;
}

extern "C" fmi3Status fmi3DeserializeFMUState(fmi3Instance instance,
                                              const fmi3Byte serializedState[],
                                              size_t size,
                                              fmi3FMUState *FMUState)
{
    return fmi3Discard;
}

extern "C" fmi3Status fmi3GetDirectionalDerivative(fmi3Instance instance,
                                                   const fmi3ValueReference unknowns[],
                                                   size_t nUnknowns,
                                                   const fmi3ValueReference knowns[],
                                                   size_t nKnowns,
                                                   const fmi3Float64 seed[],
                                                   size_t nSeed,
                                                   fmi3Float64 sensitivity[],
                                                   size_t nSensitivity)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetDirectionalDerivative"))
    {
        scilabVar vUnknown = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nUnknowns, 1);
        scilab_setUnsignedInteger32Array(NULL, vUnknown, unknowns);
        scilab_setVar(L"unknowns", vUnknown);

        scilabVar vKnown = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nKnowns, 1);
        scilab_setUnsignedInteger32Array(NULL, vKnown, knowns);
        scilab_setVar(L"knowns", vKnown);

        scilabVar vSeed = scilab_createDoubleMatrix2d(NULL, (int)nSeed, 1, 0);
        scilab_setDoubleArray(NULL, vSeed, seed);
        scilab_setVar(L"seed", vSeed);

        std::string cmd = "[sensitivity] = fmi3GetDirectionalDerivative(unknowns, knowns, seed);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetDirectionalDerivative() failed to execute.\n%s", c->formatError());
            return ret;
        }

        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"sensitivity", sensitivity, nSensitivity))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetDirectionalDerivative() return invalid sensitivity, double vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetAdjointDerivative(fmi3Instance instance,
                                               const fmi3ValueReference unknowns[],
                                               size_t nUnknowns,
                                               const fmi3ValueReference knowns[],
                                               size_t nKnowns,
                                               const fmi3Float64 seed[],
                                               size_t nSeed,
                                               fmi3Float64 sensitivity[],
                                               size_t nSensitivity)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetAdjointDerivative"))
    {
        scilabVar vUnknown = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nUnknowns, 1);
        scilab_setUnsignedInteger32Array(NULL, vUnknown, unknowns);
        scilab_setVar(L"unknowns", vUnknown);

        scilabVar vKnown = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nKnowns, 1);
        scilab_setUnsignedInteger32Array(NULL, vKnown, knowns);
        scilab_setVar(L"knowns", vKnown);

        scilabVar vSeed = scilab_createDoubleMatrix2d(NULL, (int)nSeed, 1, 0);
        scilab_setDoubleArray(NULL, vSeed, seed);
        scilab_setVar(L"seed", vSeed);

        std::string cmd = "[sensitivity] = fmi3GetAdjointDerivative(unknowns, knowns, seed);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetAdjointDerivative() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"sensitivity", sensitivity, nSensitivity))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetAdjointDerivative() return invalid sensitivity, double vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3EnterConfigurationMode(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3EnterConfigurationMode"))
    {
        std::string cmd = "fmi3EnterConfigurationMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3EnterConfigurationMode() failed to execute.\n%s", c->formatError());
            return ret;
        }

        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3ExitConfigurationMode(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3ExitConfigurationMode"))
    {
        std::string cmd = "fmi3ExitConfigurationMode();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3ExitConfigurationMode() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetIntervalDecimal(fmi3Instance instance,
                                             const fmi3ValueReference valueReferences[],
                                             size_t nValueReferences,
                                             fmi3Float64 intervals[],
                                             fmi3IntervalQualifier qualifiers[])
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetIntervalDecimal"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        // create variables to assign qualifiers to
        scilab_setVar(L"fmi3IntervalNotYetKnown", scilab_createUnsignedInteger32(NULL, fmi3IntervalNotYetKnown));
        scilab_setVar(L"fmi3IntervalUnchanged", scilab_createUnsignedInteger32(NULL, fmi3IntervalUnchanged));
        scilab_setVar(L"fmi3IntervalChanged", scilab_createUnsignedInteger32(NULL, fmi3IntervalChanged));

        std::string cmd = "[intervals, qualifiers] = fmi3GetIntervalDecimal(valueReferences);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetIntervalDecimal() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"intervals", intervals, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetIntervalDecimal() return invalid resolutions, double vector expected");
        }

        auto const mapQualifiers = call_scilab_fmu::mapVectorScilabVariable<fmi3IntervalQualifier, scilab_isUnsignedInt32, getCastTArray<fmi3IntervalQualifier, unsigned int, scilab_getUnsignedInteger32Array>>;
        if (mapQualifiers(L"qualifiers", qualifiers, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetIntervalDecimal() return invalid qualifiers, fmi3IntervalQualifier vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetIntervalFraction(fmi3Instance instance,
                                              const fmi3ValueReference valueReferences[],
                                              size_t nValueReferences,
                                              fmi3UInt64 counters[],
                                              fmi3UInt64 resolutions[],
                                              fmi3IntervalQualifier qualifiers[])
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetIntervalFraction"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        // create variables to assign qualifiers to
        scilab_setVar(L"fmi3IntervalNotYetKnown", scilab_createUnsignedInteger32(NULL, fmi3IntervalNotYetKnown));
        scilab_setVar(L"fmi3IntervalUnchanged", scilab_createUnsignedInteger32(NULL, fmi3IntervalUnchanged));
        scilab_setVar(L"fmi3IntervalChanged", scilab_createUnsignedInteger32(NULL, fmi3IntervalChanged));

        std::string cmd = "[counters, resolutions, qualifiers] = fmi3GetIntervalFraction(valueReferences);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetIntervalFraction() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        auto const mapCounters = call_scilab_fmu::mapVectorScilabVariable<fmi3UInt64, scilab_isUnsignedInt64, getCastTArray<fmi3UInt64, unsigned long long, scilab_getUnsignedInteger64Array>>;
        if (mapCounters(L"counters", counters, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetIntervalFraction() return invalid counters, uint64 vector expected");
        }

        auto const mapResolutions = call_scilab_fmu::mapVectorScilabVariable<fmi3UInt64, scilab_isUnsignedInt64, getCastTArray<fmi3UInt64, unsigned long long, scilab_getUnsignedInteger64Array>>;
        if (mapResolutions(L"resolutions", resolutions, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetIntervalFraction() return invalid resolutions, uint64 vector expected");
        }

        auto const mapQualifiers = call_scilab_fmu::mapVectorScilabVariable<fmi3IntervalQualifier, scilab_isUnsignedInt32, getCastTArray<fmi3IntervalQualifier, unsigned int, scilab_getUnsignedInteger32Array>>;
        if (mapQualifiers(L"qualifiers", qualifiers, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetIntervalFraction() return invalid qualifiers, fmi3IntervalQualifier vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetShiftDecimal(fmi3Instance instance,
                                          const fmi3ValueReference valueReferences[],
                                          size_t nValueReferences,
                                          fmi3Float64 shifts[])
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetShiftDecimal"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        std::string cmd = "[shifts] = fmi3GetShiftDecimal(valueReferences);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftDecimal() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (call_scilab_fmu::mapDoubleVectorScilabVariable(L"shifts", shifts, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftDecimal() return invalid shifts, double vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3GetShiftFraction(fmi3Instance instance,
                                           const fmi3ValueReference valueReferences[],
                                           size_t nValueReferences,
                                           fmi3UInt64 counters[],
                                           fmi3UInt64 resolutions[])
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3GetShiftFraction"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        std::string cmd = "[counters, resolutions] = fmi3GetShiftFraction(valueReferences);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        auto const mapCounters = call_scilab_fmu::mapVectorScilabVariable<fmi3UInt64, scilab_isUnsignedInt64, getCastTArray<fmi3UInt64, unsigned long long, scilab_getUnsignedInteger64Array>>;
        if (mapCounters(L"counters", counters, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() return invalid counters, uint64 vector expected");
        }

        auto const mapResolutions = call_scilab_fmu::mapVectorScilabVariable<fmi3UInt64, scilab_isUnsignedInt64, getCastTArray<fmi3UInt64, unsigned long long, scilab_getUnsignedInteger64Array>>;
        if (mapResolutions(L"resolutions", resolutions, nValueReferences))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() return invalid resolutions, uint64 vector expected");
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3SetIntervalDecimal(fmi3Instance instance,
                                             const fmi3ValueReference valueReferences[],
                                             size_t nValueReferences,
                                             const fmi3Float64 intervals[])
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3SetIntervalDecimal"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        scilabVar vIntervals = scilab_createDoubleMatrix2d(NULL, (int)nValueReferences, 1, 0);
        scilab_setDoubleArray(NULL, vIntervals, intervals);
        scilab_setVar(L"intervals", vIntervals);

        std::string cmd = "fmi3SetIntervalDecimal(valueReferences, intervals);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3SetIntervalDecimal() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3SetIntervalFraction(fmi3Instance instance,
                                              const fmi3ValueReference valueReferences[],
                                              size_t nValueReferences,
                                              const fmi3UInt64 counters[],
                                              const fmi3UInt64 resolutions[])
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3SetIntervalFraction"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        scilabVar vCounters = scilab_createUnsignedInteger64Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger64Array(NULL, vCounters, (unsigned long long *)counters);
        scilab_setVar(L"counters", vCounters);

        scilabVar vResolutions = scilab_createUnsignedInteger64Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger64Array(NULL, vResolutions, (unsigned long long *)resolutions);
        scilab_setVar(L"resolutions", vResolutions);

        std::string cmd = "fmi3SetIntervalFraction(valueReferences, counters, resolutions);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3SetIntervalFraction() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3SetShiftDecimal(fmi3Instance instance,
                                          const fmi3ValueReference valueReferences[],
                                          size_t nValueReferences,
                                          const fmi3Float64 shifts[])
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3SetShiftDecimal"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        scilabVar vShifts = scilab_createDoubleMatrix2d(NULL, (int)nValueReferences, 1, 0);
        scilab_setDoubleArray(NULL, vShifts, shifts);
        scilab_setVar(L"shifts", vShifts);

        std::string cmd = "fmi3SetShiftDecimal(valueReferences, shifts);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3SetShiftDecimal() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3SetShiftFraction(fmi3Instance instance,
                                           const fmi3ValueReference valueReferences[],
                                           size_t nValueReferences,
                                           const fmi3UInt64 counters[],
                                           const fmi3UInt64 resolutions[])
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3SetShiftFraction"))
    {
        scilabVar vValueReferences = scilab_createUnsignedInteger32Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger32Array(NULL, vValueReferences, valueReferences);
        scilab_setVar(L"valueReferences", vValueReferences);

        scilabVar vCounters = scilab_createUnsignedInteger64Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger64Array(NULL, vCounters, (unsigned long long *)counters);
        scilab_setVar(L"counters", vCounters);

        scilabVar vResolutions = scilab_createUnsignedInteger64Matrix2d(NULL, (int)nValueReferences, 1);
        scilab_setUnsignedInteger64Array(NULL, vResolutions, (unsigned long long *)resolutions);
        scilab_setVar(L"resolutions", vResolutions);

        std::string cmd = "fmi3SetShiftFraction(valueReferences, counters, resolutions);";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3SetShiftFraction() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3EvaluateDiscreteStates(fmi3Instance instance)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3EvaluateDiscreteStates"))
    {
        std::string cmd = "fmi3EvaluateDiscreteStates();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3EvaluateDiscreteStates() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }
    }

    return ret;
}

extern "C" fmi3Status fmi3UpdateDiscreteStates(fmi3Instance instance,
                                               fmi3Boolean *discreteStatesNeedUpdate,
                                               fmi3Boolean *terminateSimulation,
                                               fmi3Boolean *nominalsOfContinuousStatesChanged,
                                               fmi3Boolean *valuesOfContinuousStatesChanged,
                                               fmi3Boolean *nextEventTimeDefined,
                                               fmi3Float64 *nextEventTime)
{
    auto c = (CallScilabComponent *)instance;
    fmi3Status ret = fmi3OK;

    if (scilab_getVar(L"fmi3UpdateDiscreteStates"))
    {
        std::string cmd = "[discreteStatesNeedUpdate, terminateSimulation, nominalsOfContinuousStatesChanged, valuesOfContinuousStatesChanged, nextEventTimeDefined, nextEventTime] = fmi3UpdateDiscreteStates();";
        if (c->ExecuteScilabCommand(cmd))
        {
            ret = report<fmi3Error>(c, ret, "fmi3UpdateDiscreteStates() failed to execute.\n%s", c->formatError());
            return ret;
        }
        if (c->loggingOn)
        {
            c->logger(c->componentEnvironment, fmi3OK, NULL, c->logger_buffer.data());
        }

        if (mapBooleanScilabVariable(L"discreteStatesNeedUpdate", discreteStatesNeedUpdate))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() returned an invalid discreteStatesNeedUpdate, boolean expected");
        }
        if (mapBooleanScilabVariable(L"terminateSimulation", terminateSimulation))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() returned an invalid terminateSimulation, boolean expected");
        }
        if (mapBooleanScilabVariable(L"nominalsOfContinuousStatesChanged", nominalsOfContinuousStatesChanged))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() returned an invalid nominalsOfContinuousStatesChanged, boolean expected");
        }
        if (mapBooleanScilabVariable(L"valuesOfContinuousStatesChanged", valuesOfContinuousStatesChanged))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() returned an invalid valuesOfContinuousStatesChanged, boolean expected");
        }
        if (mapBooleanScilabVariable(L"nextEventTimeDefined", nextEventTimeDefined))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() returned an invalid nextEventTimeDefined, boolean expected");
        }
        if (call_scilab_fmu::mapScalarScilabVariable<double>(L"nextEventTime", scilab_isDouble, scilab_getDouble, nextEventTime))
        {
            ret = report<fmi3Error>(c, ret, "fmi3GetShiftFraction() returned an invalid nextEventTime, double expected");
        }
    }

    return ret;
}
