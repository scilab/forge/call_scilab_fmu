#ifndef __FMU3_HXX__
#define __FMU3_HXX__

extern "C"
{
#include "fmi3PlatformTypes.h"
#include "fmi3Functions.h"

#include <call_scilab.h>
#include <api_scilab.h>
#include <charEncoding.h>
#include <version.h>
}

#include <new>
#include <string>
#include <vector>
#include <filesystem>

#include "fmu_shared.hxx"

namespace call_scilab_fmu
{
    namespace fmu3
    {

        typedef call_scilab_fmu::CallScilabComponent<fmi3LogMessageCallback, fmi3InstanceEnvironment> CallScilabComponent;

        inline void report(fmi3LogMessageCallback fmi3LogMessageCallback, fmi3InstanceEnvironment instanceEnvironment, fmi3Status status, const char *format, ...)
        {
            std::va_list args;
            va_start(args, format);

            std::string msg(256, '\0');
            int len = std::vsnprintf(msg.data(), msg.length(), format, args);
            if (len > 0 && (size_t)len > msg.length() - 1)
            {
                msg.resize(len + 1, '\0');
                len = std::snprintf(msg.data(), msg.length(), format, args);
            }

            va_end(args);

            fmi3LogMessageCallback(instanceEnvironment, status, NULL, msg.data());
        }

        template <fmi3Status level>
        fmi3Status report(CallScilabComponent *c, fmi3Status ret, const char *format, ...)
        {
            std::va_list args;
            va_start(args, format);

            std::string msg(256, '\0');
            int len = std::vsnprintf(msg.data(), msg.length(), format, args);
            if (len > 0 && (size_t)len > msg.length() - 1)
            {
                msg.resize(len + 1, '\0');
                len = std::snprintf(msg.data(), msg.length(), format, args);
            }

            va_end(args);

            c->logger(c->componentEnvironment, level, NULL, msg.data());
            if (ret < level)
                ret = level;
            return ret;
        }

        inline int mapBooleanScilabVariable(const wchar_t *wideVarName, fmi3Boolean *out)
        {
            int temp = 0;
            int ret = mapScalarScilabVariable<int>(wideVarName, scilab_isBoolean, scilab_getBoolean, &temp);
            *out = temp != 0;
            return ret;
        }

        inline int mapSizeTScilabVariable(const wchar_t *wideVarName, size_t *out)
        {
            double temp = 0;
            int ret = mapScalarScilabVariable<double>(wideVarName, scilab_isDouble, scilab_getDouble, &temp);
            *out = (size_t)temp;
            return ret;
        }

        // helper to get data, this will copy the buffer to match fmuT and sciT
        template <typename fmuT, typename sciT, scilabStatus getFun(scilabEnv, scilabVar, sciT **)>
        scilabStatus getTArray(scilabEnv env, scilabVar var, fmuT **vals)
        {
            static std::unique_ptr<fmuT[]> temp;

            sciT *returned;
            if (getFun(env, var, &returned))
            {
                return STATUS_ERROR;
            }
            int len = scilab_getSize(env, var);
            temp = std::make_unique<fmuT[]>(len);
            for (int i = 0; i < len; ++i)
            {
                temp[i] = (fmuT)returned[i];
            }

            *vals = temp.get();
            return STATUS_OK;
        }

        // helper to cast data without copying
        template <typename fmuT, typename sciT, scilabStatus getFun(scilabEnv, scilabVar, sciT **)>
        scilabStatus getCastTArray(scilabEnv env, scilabVar var, fmuT **vals)
        {
            sciT *returned;
            if (getFun(env, var, &returned))
            {
                return STATUS_ERROR;
            }

            *vals = (fmuT *)returned;
            return STATUS_OK;
        }

        // helper to set data, this will copy the buffer to match fmuT and sciT
        template <typename fmuT, typename sciT, scilabStatus setFun(scilabEnv, scilabVar, const sciT *)>
        scilabStatus setTArray(scilabEnv env, scilabVar var, const fmuT *vals)
        {
            int len = scilab_getSize(env, var);
            std::unique_ptr<sciT[]> temp = std::make_unique<sciT[]>(len);
            for (int i = 0; i < len; ++i)
            {
                temp[i] = vals[i];
            }
            return setFun(env, var, temp.get());
        }

        // helper to cast data without copying
        template <typename fmuT, typename sciT, scilabStatus setFun(scilabEnv, scilabVar, const sciT *)>
        scilabStatus setCastTArray(scilabEnv env, scilabVar var, const fmuT *vals)
        {
            return setFun(env, var, (sciT *)vals);
        }

        // typesafe helper to create data, fmuT will be promoted to sciT by assignement operator
        template <typename fmuT, typename sciT, scilabVar fun(scilabEnv, sciT)>
        scilabVar createT(scilabEnv env, fmuT v)
        {
            return fun(env, v);
        }

    } /* namespace fmu3 */
}     /* namespace call_scilab_fmu */

#endif /* __FMU3_HXX__ */