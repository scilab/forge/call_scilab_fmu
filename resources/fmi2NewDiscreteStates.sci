function [newDiscreteStatesNeeded, terminateSimulation, nominalsOfContinuousStatesChanged, valuesOfContinuousStatesChanged, nextEventTimeDefined, nextEventTime] = fmi2NewDiscreteStates()
    newDiscreteStatesNeeded = %f;
    terminateSimulation = %f;
    nominalsOfContinuousStatesChanged = %f;
    valuesOfContinuousStatesChanged = %f;
    nextEventTimeDefined = %f;
    nextEventTime = 0;
endfunction
