function [discreteStatesNeedUpdate, terminateSimulation, nominalsOfContinuousStatesChanged, valuesOfContinuousStatesChanged, nextEventTimeDefined, nextEventTime] = fmi3UpdateDiscreteStates()
    discreteStatesNeedUpdate = %f
    terminateSimulation = %f
    nominalsOfContinuousStatesChanged = %f
    valuesOfContinuousStatesChanged = %f
    nextEventTimeDefined = %f
    nextEventTime = 0
endfunction
