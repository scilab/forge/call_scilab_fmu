function ModelVariables = fmi3ActivateModelPartition(ModelVariables)
    disp("ModelVariables", ModelVariables, ..
         "activationTime", activationTime);

    // double 
    ModelVariables(8) = ModelVariables(7) + ModelVariables(5) + ModelVariables(6) + 1;

    // int32
    ModelVariables(20) = ModelVariables(19) + int32(1);
    
    // boolean
    ModelVariables(28) = ~ ModelVariables(27);

    // enumeration
    ModelVariables(33) = ModelVariables(32);
endfunction
