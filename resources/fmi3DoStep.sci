function [ModelVariables] = fmi3DoStep(ModelVariables)
    disp("ModelVariables", ModelVariables, ..
         "currentCommunicationPoint", currentCommunicationPoint, ..
         "communicationStepSize", communicationStepSize, ..
         "noSetFMUStatePriorToCurrentPoint", noSetFMUStatePriorToCurrentPoint);

    // double 
    ModelVariables(8) = ModelVariables(7) + ModelVariables(5) + ModelVariables(6) + 1;

    // int32
    ModelVariables(20) = ModelVariables(19) + int32(1);
    
    // boolean
    ModelVariables(28) = ~ ModelVariables(27);

    // enumeration
    ModelVariables(33) = ModelVariables(32);


    // setting a custom simulation status is possible with one of the following variable
    eventHandlingNeeded = %f;
    terminateSimulation = %f;
    earlyReturn = %f;
    lastSuccessfulTime = currentCommunicationPoint;
    [eventHandlingNeeded, terminateSimulation, earlyReturn, lastSuccessfulTime] = resume(eventHandlingNeeded, terminateSimulation, earlyReturn, lastSuccessfulTime)
endfunction
