
set(SCI "" CACHE PATH "Path to Scilab installation")
message(CHECK_START "Detecting Scilab")

list(APPEND CMAKE_MESSAGE_INDENT "  ")

message(CHECK_START "cache entry SCI")
if(EXISTS "${SCI}")
    message(CHECK_PASS "found")
else()
    message(CHECK_FAIL "not set")
endif()

if(NOT EXISTS "${SCI}")
    message(CHECK_START "env. variable SCI")
    if(DEFINED ENV{SCI})
        if(EXISTS "$ENV{SCI}")
            message(CHECK_PASS "found \"$ENV{SCI}\"")
            set(SCI "$ENV{SCI}")
        else()
            message(CHECK_FAIL "ignored")
        endif()
    else()
        message(CHECK_FAIL "not set")
    endif()
endif()

if(NOT EXISTS "${SCI}")
    message(CHECK_START "on the PATH")
    find_program(SCIBINARY scilab REQUIRED)
    cmake_path(GET SCIBINARY PARENT_PATH SCIBIN)
    cmake_path(GET SCIBIN PARENT_PATH SCI)
    message(CHECK_PASS "found")
endif()

list(POP_BACK CMAKE_MESSAGE_INDENT)
message(CHECK_PASS "set to \"${SCI}\"")

add_library(Scilab INTERFACE IMPORTED)
if(WIN32)
target_include_directories(Scilab INTERFACE
    "${SCI}/modules/call_scilab/includes"
    "${SCI}/modules/core/includes"
    "${SCI}/modules/api_scilab/includes"
    "${SCI}/modules/ast/includes/ast"
    "${SCI}/modules/ast/includes/types"
    "${SCI}/modules/dynamic_link/includes"
    "${SCI}/modules/string/includes"
    "${SCI}/modules/fileio/includes"
    "${SCI}/modules/localization/includes"
    "${SCI}/modules/output_stream/includes"
    "${SCI}/libs/intl"
)
target_link_libraries(Scilab INTERFACE
    "${SCI}/bin/call_scilab.lib"
    "${SCI}/bin/core.lib"
    "${SCI}/bin/api_scilab.lib"
    "${SCI}/bin/ast.lib"
    "${SCI}/bin/fileio.lib"
    "${SCI}/bin/scilocalization.lib"
    "${SCI}/bin/output_stream.lib"
    "${SCI}/bin/libintl.lib"
)
elseif(UNIX)
target_include_directories(Scilab INTERFACE
    "${SCI}/include/scilab"
)
target_link_libraries(Scilab INTERFACE
    "${SCI}/lib/scilab/libscicall_scilab.so"
    "${SCI}/lib/scilab/libscilab.so"
)
endif()
