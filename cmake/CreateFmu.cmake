# create a Scilab FMU for a TARGET with some Scilab source FILES
macro(create_fmu)
  set(options "")
  set(oneValueArgs TARGET FMI_VERSION)
  set(multiValueArgs FILES)
  cmake_parse_arguments(CREATE_FMU "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  # Select the right binary directory for the output
  if(CREATE_FMU_FMI_VERSION EQUAL "2")
    if(UNIX AND NOT WIN32)
      if(CMAKE_SIZEOF_VOID_P MATCHES "8")
        set(FMU_BINARY_DIRECTORY binaries/linux64)
      else()
        set(FMU_BINARY_DIRECTORY binaries/linux32)
      endif()
      else()
      if(CMAKE_SIZEOF_VOID_P MATCHES "8")
        set(FMU_BINARY_DIRECTORY binaries/win64)
      else()
        set(FMU_BINARY_DIRECTORY binaries/win32)
      endif()
    endif()
  elseif(CREATE_FMU_FMI_VERSION EQUAL "3")
    if(UNIX AND NOT WIN32)
      if(CMAKE_SIZEOF_VOID_P MATCHES "8")
        set(FMU_BINARY_DIRECTORY binaries/x86_64-linux)
      else()
        set(FMU_BINARY_DIRECTORY binaries/x86-linux)
      endif()
    else()
      if(CMAKE_SIZEOF_VOID_P MATCHES "8")
        set(FMU_BINARY_DIRECTORY binaries/x86_64-windows)
      else()
        set(FMU_BINARY_DIRECTORY binaries/x86-windows)
      endif()
    endif()
  else()
    message(FATAL_ERROR "create_fmu() needs an FMI_VERSION argument")
  endif()
  
  add_custom_command(TARGET ${CREATE_FMU_TARGET}
      POST_BUILD
      COMMAND
      ${CMAKE_COMMAND} -E "make_directory"
          "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}"
          "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}/${FMU_BINARY_DIRECTORY}"
          "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}/resources"
          "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}/documentation"
  )
  add_custom_command(TARGET ${CREATE_FMU_TARGET}
      POST_BUILD
      COMMAND
      ${CMAKE_COMMAND} -E "copy"
          "${CMAKE_CURRENT_SOURCE_DIR}/documentation/index.html"
          "${CMAKE_CURRENT_SOURCE_DIR}/documentation/externalDependencies.html"
          "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}/documentation/"
  )
  add_custom_command(TARGET ${CREATE_FMU_TARGET}
      POST_BUILD
      COMMAND
      ${CMAKE_COMMAND} -E "copy"
          "${CMAKE_BINARY_DIR}/${CREATE_FMU_TARGET}/modelDescription.xml"
          "${CMAKE_CURRENT_SOURCE_DIR}/model.png"
          "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}"
  )
  foreach(FILE IN LISTS CREATE_FMU_FILES)
    add_custom_command(TARGET ${CREATE_FMU_TARGET}
        POST_BUILD
        COMMAND
        ${CMAKE_COMMAND} -E "copy"
            "${CMAKE_CURRENT_SOURCE_DIR}/${FILE}"
            "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}/${FILE}"
    )
  endforeach()
  
  add_custom_command(TARGET ${CREATE_FMU_TARGET}
      POST_BUILD
      COMMAND
      ${CMAKE_COMMAND} -E "copy"
          "$<TARGET_FILE:${CREATE_FMU_TARGET}>"
          "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}/${FMU_BINARY_DIRECTORY}/${CREATE_FMU_TARGET}${CMAKE_SHARED_LIBRARY_SUFFIX}"
  )
  add_custom_command(TARGET ${CREATE_FMU_TARGET}
      POST_BUILD
      COMMAND
      ${CMAKE_COMMAND} -E "tar" "cvf" "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}.fmu"
          "--format=zip"
          "modelDescription.xml"
          "binaries/"
          "resources/"
          "documentation/"
          "model.png"
      WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/FMUs/${CREATE_FMU_TARGET}"
      COMMENT "FMUs/${CREATE_FMU_TARGET}.fmu content"
  )
endmacro()
